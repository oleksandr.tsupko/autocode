package Arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class SortingArr {
    public static void main(String[] args) {

        int[] arr = {9, 8, 6, 1, 3};
        sortArr(arr);
    }

    public static void sortArr(int[] arr){

        int variab = 0;
        boolean sortComplete = true;
        while (sortComplete){
            sortComplete = false;
            for (int i = 0; i < arr.length-1; i++) {
                if (arr[i] > arr[i+1]){
                    variab = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = variab;
                    sortComplete = true;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
